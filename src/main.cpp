#include <iostream>
#include <csignal>
#include <memory>
#include <thread>

#include "ServerThread.h"
#include "Client.h"
#include "CliThread.h"

ServerThread st;

void BreakSignalHandler(int signal);

int main ()
{
    std::signal(SIGINT, BreakSignalHandler);
    CliThread clit(st.GetClientList());
    std::thread server_thread = st.GetThreadHandler();
    std::thread cli_thread = clit.GetThreadHandler();
    server_thread.join();
	std::cout << "Server thread has terminated" << std::endl;
    clit.ExitFlag = true;
    cli_thread.join();
    return 0;
}

void BreakSignalHandler(int signal)
{
    st.ExitFlag = true;
}
