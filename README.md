Dependencies:
    libsfml-*
    libboost-all-dev
    cmake
    make
    g++
    
Building:
    mkdir build
    cmake .. <path-to-cloned-repo>
    make

Running:
    ./CentralServer


