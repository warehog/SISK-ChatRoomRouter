#ifndef SERVERTHREAD_H
#define SERVERTHREAD_H
#include <stdio.h>
#include <vector>
#include <thread>
#include <csignal>
#include <iostream>

#include "Client.h"
#include "Message.h"

class ServerThread
{
    public:
        ServerThread();
        virtual ~ServerThread();
        unsigned int GetPort() { return m_Port; }
        void SetPort(unsigned int val) { m_Port = val; }
        std::thread GetThreadHandler();
        bool ExitFlag = false;
        std::shared_ptr<std::vector<Client*>> ClientList;
        std::shared_ptr<std::vector<Client*>> GetClientList();
		std::string random_string( size_t length );
		Message GetMesssage(std::string load);
		void HandleMessage(Message msg, Client *sender);
		std::vector<std::string> chat_rooms;
		void NameSetMessageHandle(Message msg, Client *sender);
		void RoomCreateMessageHandle(Message msg, Client *sender);
		void RoomJoinMessageHandle(Message msg, Client *sender);
		void ListUsersMessageHandle(Message msg, Client *sender);
		void ListChatsMessageHandle(Message msg, Client *sender);
		void RoomSendMessageHandle(Message msg, Client *sender);
    protected:

    private:

        unsigned int m_Port;
        void ServerThreadWorker();


};

#endif // SERVERTHREAD_H
