#ifndef CLIENT_H
#define CLIENT_H

#include <string>
#include <iostream>

#include <SFML/Network.hpp>

class Client
{
    public:
        Client();
        virtual ~Client();

        std::string GetIpAddr() { return ip_addr; }
        void SetIpAddr(std::string val) { ip_addr = val; }
        std::string GetName() { return name; }
        void SetName(std::string val) { name = val; }
        unsigned int GetId() { return id; }
        void SetId(unsigned int val) { id = val; }
        sf::TcpSocket Socket;
        bool IsAlive();
        void Send(char *val);
        void Send(std::string val);
		std::string room_name;
		bool name_set = false;
		bool chat_set = false;
    protected:

    private:
        std::string ip_addr;
        std::string name;
		
        unsigned int id;

};

#endif // CLIENT_H
